/* jshint node: true */
/* jshint esversion: 6 */

const rnd = {
    getNormalRandom: (min, max) => {
        if (min > max) {
            const t = min;
            min = max;
            max = t;
        } else if (min === max) {
            console.warn('RND >> Specified min is equal to max. Returning zero as default.');
            return 0;
        }

        const range = max - min;
        const r1 = Math.random(),
            r2 = Math.random(),
            r3 = Math.random();
        const random1 = Math.sqrt(0.33 * (r1 * r1 + r2 * r2 + r3 * r3));
        const result = min + random1 * range;

        return result;
    }
};

export default rnd;