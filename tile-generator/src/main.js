/* jshint node: true */
/* jshint esversion: 6 */

import Vue from 'vue';
import App from './App.vue';
import { observable, isObservable, toJS } from 'mobx';
import Vuex from 'vuex';

new Vue({
    el: '#app',
    render: h => h(App)
})